#!/bin/sh
set -ex
PROJECT=gdb

apt source $PROJECT
WORKDIR=$(echo $PROJECT-*)
cd $WORKDIR

if [ -z "$VERSION_HEADER" ] ; then
	echo VERSION_HEADER is not defined
	exit 1
fi

SEDOUT="-i debian/rules"
# Find any append to the configure arguments and insert our mixture
sed -e "s|--with-babeltrace|--without-babeltrace|" $SEDOUT
sed -e "s|--with-intel-pt|--without-intel-pt|" $SEDOUT
sed -e "s|--with-lzma|--without-lzma|" $SEDOUT
sed -e "s|--with-mpfr|--without-mpfr|" $SEDOUT
sed -e "s|--with-system-readline|--without-system-readline|" $SEDOUT
sed -e "s|--disable-readline|--enable-readline|" $SEDOUT
sed -e "s|--enable-tui|--disable-tui|" $SEDOUT
sed -e "s|tinfo||" $SEDOUT
#only one occurence of with-expat
sed -e "s|--with-expat|--with-expat=no --without-system-readline --disable-tui --enable-static --disable-shared --without-mpfr|" $SEDOUT

sed -e "0,/^CPPFLAGS += -fPIC/s||\
CFLAGS += -include $VERSION_HEADER\n&|" $SEDOUT

patch -p1 < ../gdb.patch

ac_cv_search_tgetent=no dpkg-buildpackage -b -us -uc

