#!/bin/bash
set -eux

ARCH=$1
TRIPLET=$2
TOOLCHAIN=apertis-$TRIPLET-toolchain

cd $(dirname $0)

upload_host="archive@images.apertis.org"
upload_port="7711"
eval $(grep VERSION_CODENAME= /etc/os-release)
upload_root="/srv/images/public/daily/$VERSION_CODENAME"
upload_dest="$upload_host:$upload_root"
target="toolchain"

apt -y update
apt -y install rsync ssh
PIPELINE_VERSION=${PIPELINE_VERSION:-$(date +%Y%m%d.%H%M)}
chmod 0400 "$ARCHIVE_SECRET_FILE"
ssh -oStrictHostKeyChecking=no -p "$upload_port" -i "$ARCHIVE_SECRET_FILE" "$upload_host" mkdir -p "$upload_root/$PIPELINE_VERSION/$target/"
rsync -e "ssh -p $upload_port -oStrictHostKeyChecking=no -i $ARCHIVE_SECRET_FILE" -aP $TOOLCHAIN.tar.xz "$upload_dest/$PIPELINE_VERSION/$target/"
