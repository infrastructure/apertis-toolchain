#!/bin/sh
set -ex
PROJECT=gcc-12-cross

ARCH=${1-armhf}
TRIPLET=${2-arm-linux-gnueabihf}

apt source $PROJECT
WORKDIR=$(echo $PROJECT-*)
cd $WORKDIR

curdir=$(pwd)

# Put the static libraries in a single folder
STATIC_LIBS=$curdir/static-libs
SOURCE_DIR=/usr/lib/x86_64-linux-gnu
rm -rf $STATIC_LIBS
mkdir $STATIC_LIBS
cp \
	$SOURCE_DIR/libisl.a \
	$SOURCE_DIR/libmpc.a  \
	$SOURCE_DIR/libmpfr.a \
	$SOURCE_DIR/libgmp.a  \
	$STATIC_LIBS

# Set our configuration files in the gcc source
cp -R /usr/src/gcc-12 .
SEDOUT="-i gcc-12/debian/rules2"
# Find any append to the configure arguments and insert our mixture
sed -e "0,/^CONFARGS +=/s||\
CONFARGS += --with-isl-lib=$STATIC_LIBS --with-mpfr-lib=$STATIC_LIBS --with-mpc-lib=$STATIC_LIBS --with-gmp-lib=$STATIC_LIBS\n&|" $SEDOUT
sed -e "s|--with-as=.*||" $SEDOUT
sed -e "s|--with-ld=.*||" $SEDOUT

SEDOUT="-i gcc-12/debian/rules.defs"
sed -e "s|with_ada := yes|with_ada := no|" $SEDOUT
sed -e "s|with_objc := yes|with_objc := no|" $SEDOUT
sed -e "s|with_fortran := yes|with_fortran := no|" $SEDOUT
sed -e "s|with_go := yes|with_go := no|" $SEDOUT
sed -e "s|with_d := yes|with_d := no|" $SEDOUT

# Build the gcc package from the modified gcc source
SEDOUT="-i debian/rules"
sed -e "s|/usr/src/gcc-|$(pwd)/gcc-|" $SEDOUT
# Sysroot must be under prefix to be relocatable
sed -e "s|WITH_SYSROOT=/|WITH_SYSROOT=/usr/.. WITH_BUILD_SYSROOT=/ |" $SEDOUT
#sed -e "s|WITHOUT_LANG=\"|WITHOUT_LANG=\"ada fortran go |" $SEDOUT


#CROSS_ARCHS=$ARCH dpkg-buildpackage -b -us -uc
CROSS_ARCHS=$ARCH dpkg-buildpackage -b -j$(nproc) -us -uc

rm -f *gnat*.deb *fortran*.deb
