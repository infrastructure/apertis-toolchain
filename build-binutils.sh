#!/bin/sh
set -ex
PROJECT=binutils

ARCH=${1-armhf}
TRIPLET=${2-arm-linux-gnueabihf}

apt source $PROJECT
WORKDIR=$(echo $PROJECT-*)
cd $WORKDIR

if [ -z "$VERSION_HEADER" ] ; then
	echo VERSION_HEADER is not defined
	exit 1
fi

export LEXLIB='-l:libfl.a'

SEDOUT="-i debian/rules"
sed -e "s|CFLAGS.*-O2|& -include $VERSION_HEADER|"             $SEDOUT

patch -p1 < ../binutils.patch

TARGET=$ARCH dpkg-buildpackage -b -j$(nproc) -us -uc

